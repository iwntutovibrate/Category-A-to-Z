<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
	<title>A to Z</title>
</head>

<style>

	a {
		text-decoration:none;
		color : black;
	}

</style>

<body>

	<div id="test"></div>
	<br />
	<div id="list"></div>
	
</body>

<script type="text/javascript">

	$(document).ready(function(){
		
		asciiList();
		
	});
	
	function asciiList(){
			
		for(var i=65; i<=90; i++){
			
			var ascii = String.fromCharCode(i);
			var html = '<a href="javascript:;" onclick="goAtoz(\''+ascii+'\')">'+ascii+'</a> ';
			
			$('#test').append(html);
		}
	}
	
	function goAtoz(par){
		
		var url = "/atoz/list";
		
		$.get(url, {az : par}, function(json){
			$('#list').html(json);
		});
	};

</script>
</html>
