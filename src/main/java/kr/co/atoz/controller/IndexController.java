package kr.co.atoz.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.atoz.dto.AtozDTO;

@Controller
public class IndexController {

	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index(Model model) {
		
		return "index";
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public void Atoz(Model model, AtozDTO atoz) {
		
		model.addAttribute("list", atoz.getAz());
	}
}
